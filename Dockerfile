FROM node:16-alpine as base
WORKDIR /app
COPY package*.json ./

FROM base as test
RUN npm ci
COPY . .
CMD ["npm", "run", "test"]

FROM base as stage
RUN npm ci --production
COPY . .
CMD ["npm", "run", "start"]