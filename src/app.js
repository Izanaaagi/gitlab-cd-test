const express = require('express');

const app = express();
const router = express.Router();

const cats = [{ name: 'Sam', years: '2' }, { name: 'Tom', years: '1' }, { name: 'Mua', years: '7' }];

const dogs = [{ name: 'Doge', years: '2' }, { name: 'Hash', years: '1' }, { name: 'Gav', years: '7' }];

router.get('/test', (req, res) => {
    res.send({ message: 'Test message' });
});

router.get('/cats', (req, res) => {
    res.send({ cats });
});

router.get('/dogs', (req, res) => {
    res.send({ dogs });
});

app.use('/api', router);

module.exports = app;
