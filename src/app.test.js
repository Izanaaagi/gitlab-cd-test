const request = require('supertest');
const app = require('./app');

describe('Test routes', () => {
    it(`should return message: "test message"`, async () => {
        const res = await request(app).get('/api/test');
        expect(res.body.message).toEqual('Test message');
        expect(res.statusCode).toEqual(200);
    });

    it('should return array of cats', async () => {
        const res = await request(app).get('/api/cats');
        expect(Array.isArray(res.body.cats)).toBe(true);
    });
});
