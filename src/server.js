const http = require('http');
const app = require('./app');

const server = http.createServer(app);

const port = 5000;
const host = '0.0.0.0';

server.listen(port, host, () => {
    console.log(`Server started on path http://${ host }:${ port }`);
});

module.exports = { port, host };
